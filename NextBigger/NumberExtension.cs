﻿using System.Globalization;

namespace NextBiggerTask
{
    public static class NumberExtension
    {
        /// <summary>
        /// Finds the nearest largest integer consisting of the digits of the given positive integer number; return -1 if no such number exists.
        /// </summary>
        /// <param name="number">Source number.</param>
        /// <returns>
        /// The nearest largest integer consisting of the digits  of the given positive integer; return -1 if no such number exists.
        /// </returns>
        /// <exception cref="ArgumentException">Thrown when source number is less than 0.</exception>
        public static int NextBiggerThan(int number)
        {
            if (number < 0)
            {
                throw new ArgumentException("bad one", nameof(number));
            }
            else if (number >= int.MaxValue)
            {
                return -1;
            }

            string numString = Convert.ToString(number, CultureInfo.CurrentCulture);
            string maxVT = string.Join(string.Empty, numString.OrderByDescending(ch => ch));
            int num = number + 1, max = int.Parse(maxVT, CultureInfo.CurrentCulture);

            if (num <= 0)
            {
                throw new ArgumentException("bad one", nameof(number));
            }
            else
            {
                while (num <= max)
                {
                    if (NumsHaveSameDigits(number, num))
                    {
                        return num;
                    }

                    num++;
                }

                return -1;
            }
        }

        private static bool NumsHaveSameDigits(int num1, int num2)
        {
            string num1Text = num1.ToString(CultureInfo.CurrentCulture);
            string num2Text = num2.ToString(CultureInfo.CurrentCulture);
            return num1Text.OrderBy(ch => ch).SequenceEqual(num2Text.OrderBy(ch => ch));
        }
    }
}
